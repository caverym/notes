#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include "notes.h"

note readFromFile(FILE * fpointer, int line) {
	note return_note = * ( note *) malloc(sizeof(note));

		char buffer[1024];

		int row = 0;
		int column = 0;

		for (int i = 1; i <= line; i++) {
			fgets(buffer, 1024, fpointer);

			sscanf(buffer, "%s, %s, %s\n", return_note.title, return_note.filename, return_note.date);
		}

		printf("%s", buffer);
		
	return return_note;
}

int main(){
	printf("Debug text\n");

	FILE *fpointer = fopen(FILENAME, "rw");

	if (FILENAME == NULL) {
		perror("Unable to open file");
		exit(1);
	}

	note Note = readFromFile(fpointer, 2);

	fprintf(fpointer, "%s, %s, %s\n", Note.title, Note.filename, Note.date);

	fclose(fpointer);

	return 0;
}
